+++
title = "Links de grupos de TI no telegram"
date = "2015-11-07T10:43:38"
aliases = [ "/2015/11/07/links-de-grupos-de-ti-no-telegram/", "/post/links-de-grupos-de-ti-no-telegram/" ]
categories = [ "Sem categoria" ]
tags = [ "Grupos", "Telegram" ]
author = "somatorio"

+++
Update: O grande [Rafael Gomes](http://techfree.com.br) (@gomex) elevou a lista a um novo patamar... a lista agora é gerada no [Jekyll](https://jekyllrb.com/) a partir de um [repositório no github](https://github.com/listatelegram/listatelegram.github.io), o que isso significa? Que agora ela é mantida pela comunidade, e não mais centralizada comigo, o que é ótimo!

# [NOVO ENDEREÇO][1]


Vou manter a lista abaixo como estava no momento da transição por motivos de lembrança

* * *

Me passaram essa lista de links de convite e eu imagino que pode ser útil pra alguém procurando grupos para participar

Quando descobrir outros links atualizo aqui, caso tenha algum link que não está na lista deixe nos comentários ou fale comigo em privado ([@somatorio](http://www.telegram.me/somatorio))

**Networkers BR**  
[https://telegram.me/NetworkersBR][1]  
*O grupo surgiu no Blog CCNA, e tem pessoas de todos os níveis. Então o foco principal são as tecnologias e certificações da Cisco. Outros assuntos, desde que sejam interessantes, produtivos e que agreguem valor ao grupo são bem-vindos.  
Leia as regras!*  
Contribuição: Igor M. de Assis (<a href="https://telegram.me/igormassis" target="_blank">@igormassis</a>)

**Mikrotik Brasil**  
[https://telegram.me/mikrotikbr][1]   
Contribuição: Roberto Brito (@reustaquio](http://www.telegram.me/reustaquio))

**Sempre Update Linux**  
[https://telegram.me/joinchat/AbDu9jv9mMuEmOyk-wvIGw][1]

**pfSense BR**  
[https://telegram.me/joinchat/A733uTwnFgxq3xG_5Dzhng][1]  
Contribuição: Júlio Saraiva ([@Julinux](http://www.telegram.me/Julinux))

**Infraestrutura TI**  
[https://telegram.me/infraestruturati][1]  
Contribuição: Crystian Geovani Dorabiatto ([@Dorabiatto](http://www.telegram.me/Dorabiatto))

**OpenStack**  
[https://telegram.me/joinchat/A733uQEc9QeO044adfcbgQ][1]  
Contribuição: Júlio Saraiva ([@Julinux](http://www.telegram.me/Julinux))

**Samba4**  
[https://telegram.me/joinchat/BtO01gI_MmSflgUf3liF-g][1]

**Proxmox VE**  
[http://telegram.me/ProxmoxBrasil][1]

**Citrix XenServer**  
[https://telegram.me/joinchat/047dd738016341fabbdb541f55cd89a1][1]

**Zabbix Brasil**  
[https://telegram.me/joinchat/AI7jrT05Fqe8BscujO5IfQ][1]

**docker-users-br**  
[https://telegram.me/dockerbr][1]  
Colaboração: Rafael Gomes ([@gomex](http://www.telegram.me/gomex))

**Virtualização**  
[https://telegram.me/joinchat/AI7jrT5lBV9gFGE3w-Fy6Q][1]  
Contribuição: Jozafá Soares ([@Jozafa](http://www.telegram.me/Jozafa))

**Bacula-BR**  
[https://telegram.me/baculabr][1]  
Contribuição: Anderson Vieira ([@andersonmv](http://telegram.me/andersonmv))

**C'users (programação em C)**  
[https://telegram.me/joinchat/B9C6HQLFGOwlCUTSJdaE7A][1]

**Arduino, Raspberry Eletrônica e Afins...**  
[https://telegram.me/joinchat/BQwnFTxx069EQRiYyIiLJw][1]  
Contribuição: André Luiz Alves ([@andrentfs](http://www.telegram.me/andrentfs))

**Asterisk Elastix Freepbx Piaf**  
[https://telegram.me/joinchat/CMF1cQNxp6Xx0pj-RteImA][1]

**Glpi**  
[https://telegram.me/joinchat/BH3XOAC4F68mzj-tKc6xoQ][1]  
Contribuição: Kleber Ribeiro ([@nk3info](http://www.telegram.me/nk3info))

**Cloudstack**  
[https://telegram.me/joinchat/AxrvtASPySwVlMekVc1u9g][1]  
Contribuição: Tales Santos ([@Talesteixeira](http://www.telegram.me/Talesteixeira))

**DevOps BR**  
[https://telegram.me/joinchat/AxrvtAOCKuIHCih17PT7Qg][1]  
Contribuição: Tales Santos ([@Talesteixeira](http://www.telegram.me/Talesteixeira))

**Linux + LPI**  
[https://telegram.me/joinchat/CYyDAT0Jg_EVAEedD9qgsw][1]  
Bot para estudos do grupo "Linux + LPI": [https://telegram.me/GrupoLinuxbot][1]  
Contribuição: Reginaldo ([@Saitam10](http://www.telegram.me/Saitam10))

**PHP para Asterisk**  
[https://telegram.me/joinchat/A2dO3gR-WUwZv89DFzJTEw][1]  
Contribuição: Angelo Delphini ([@AngeloDelphini](http://www.telegram.me/AngeloDelphini))

**CCNA Brasil**  
[https://telegram.me/ccnabrasil][1]  
*Grupo OFICIAL no Telegram da Comunidade CCNA BRASIL no FB (<https://www.facebook.com/groups/CCNA.Brasil/>).  
Somente assuntos relacionados à certificações CISCO, como CCNA, CCNP e CCIE.*  
Contribuição: Tiago Lima ([@tilima](http://www.telegram.me/tilima))

**Empreendedorismo TI**  
[https://telegram.me/joinchat/AI7jrT4ZFk94UZco3ymVsQ][1]  
Contribuição: Jozafá Soares ([@Jozafa](http://www.telegram.me/Jozafa))

**Zimbrasil (zimbra)**  
[https://telegram.me/zimBrasil][1]  
Contribuição: Werneck Costa ([@WerneckCosta](http://www.telegram.me/WerneckCosta))

**PHP Maranhão UG**  
[https://telegram.me/joinchat/AOBqkQB3Pt6YVANKgdh7IA][1]  
Contribuição: Bruno Emanuel Silva ([@jwalker_pe](http://www.telegram.me/jwalker_pe))

**LinuxBRAZUCA**  
[https://telegram.me/joinchat/Aguf-Duk8yp2vI7hNywbHQ][1]

**Planeta Linux (canal)**  
[https://telegram.me/planetalinux][1]

**NxFilter - DNS WebFilter**  
[https://telegram.me/NxfilterBR][1]  
Contribuição: Sergio Lopes ([@sergiolps](http://www.telegram.me/sergiolps))

**Arch Linux Brasil**  
[https://telegram.me/joinchat/Ca-T5j01ENns1TC7Dpaimw][1]  
Contribuição: Alan Cads ([@alancads](http://www.telegram.me/alancads))

**DBA Brasil**  
[https://telegram.me/joinchat/BSo6ET1rO4Ba2eSOHyMhGg][1]  
Contribuição: Fábio Telles Rodriguez ([@fabio_telles](http://telegram.me/fabio_telles))

**Shell Script**  
[https://telegram.me/joinchat/A733uQf7AAw5DTo1kFWCFA][1]  
Contribuição: Júlio Saraiva ([@Julinux](http://www.telegram.me/Julinux))

**Expressões Regulares**  
[https://telegram.me/joinchat/BtO01j1rBi16tGTsHJ2CuQ][1]  
Contribuição: Igor Motta ([@Igmotta](http://www.telegram.me/Igmotta))

**Sharing Delphi Brasil**  
[https://telegram.me/joinchat/CBODLDxOQ58JOdNfHIRzBA][1]  
Contribuição: Felipe Sartori ([@feehzor](http://www.telegram.me/feehzor))

**Parceiros de Codigos**  
[https://telegram.me/joinchat/CEMxwATo7yv6NW2y7CCGtw][1]  
Contribuição: Felipe Sartori ([@feehzor](http://www.telegram.me/feehzor))

**FAL - Fundação Asterisk Libre**  
[https://telegram.me/falbr][1]  
*Ao entrar aqui se apresente, lembre-se que é um canal restrito para profissionais em Asterisk e Desenvolvedores de Telefonia que faz uso do Framework Asterisk. Voce pode expor produtos e serviços. Pedimos por favor que não APOIE, não INCENTIVE o uso de forks como trixbox, elastix e tal.*  
Contribuição: Fagner Silva ([@fagnersilvas](https://telegram.me/fagnersilvas))

**Usuários SnepLivre**  
[https://telegram.me/joinchat/CQ-FrggJFqo0WtkXGC1cFQ][1]  
Contribuição: Raphael Monteiro ([@pontara](https://telegram.me/pontara))

**puppet-users-br**  
[https://telegram.me/puppetbr][1]  
Contribuição: Guto Carvalho ([@gutocarvalho](http://www.telegram.me/gutocarvalho))

**gitlab-users-br**  
[https://telegram.me/joinchat/AejjFQf2XJYdTFmrCfLBsg][1]  
Contribuição: Guto Carvalho ([@gutocarvalho](http://www.telegram.me/gutocarvalho))

**vagrant-users-br**  
[https://telegram.me/joinchat/AejjFQZcA-IOjJmRXzmcIA][1]  
Contribuição: Guto Carvalho ([@gutocarvalho](http://www.telegram.me/gutocarvalho))

**elastic-users-br**  
[https://telegram.me/joinchat/AejjFQCHmDHk7796cRQXYA][1]  
Contribuição: Guto Carvalho ([@gutocarvalho](http://www.telegram.me/gutocarvalho))

**Tecnologia da Informação**  
*Grupo com o objetivo de reunir os amantes da tecnologia para trocar informações, tirar duvidas, artigos, ideias e materiais.*  
Canal: [https://telegram.me/joinchat/CBcyYj5O4p6nKLLZ3i5dYA][1]  
Grupo: [https://telegram.me/joinchat/CBcyYj32K4fEDI1BVBGagg][1]  
Contribuição: Jarvis ([@Showshock](http://www.telegram.me/Showshock))

**SecurityCast**  
[http://bit.ly/securitycast][1]  
Contribuição: Alcyon Ferreira de Souza Junior

**OpenSIPs Brasil**  
[https://telegram.me/opensipsbrasil][1]  
Contribuição: Fagner Silva ([@fagnersilvas](http://www.telegram.me/fagnersilvas))

**FreeSwitchBrasil**  
[https://telegram.me/freeswitchbrasil][1]  
Contribuição: Fagner Silva ([@fagnersilvas](http://www.telegram.me/fagnersilvas))

**Compra/Venda/Troca/Serviços de TI**  
[https://telegram.me/compravendatrocati][1]  
Contribuição: Werneck Costa ([@WerneckCosta](http://telegram.me/WerneckCosta))


[1]: http://listatelegram.github.io/
