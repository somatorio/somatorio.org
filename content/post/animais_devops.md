+++
title = "Animais DevOps: onde vivem? do que se alimentam?"
date = "2017-01-27T12:51:15"
slug = "animais_devops"
categories = [ "DevOps" ]
tags = [ "DevOps", "terminologias" ]
author = "somatorio"

+++
Não sei quanto a vocês, mas volta e meia lendo algum post/artigo/texto/whatever, em alguma palestra, ou ouvindo algum Podcast eu acabo lendo/vendo/ouvindo várias piadas/gírias (principalmente envolvendo animais por algum motivo) e fico me sentindo "o cara de fora", como se aquele texto/palestra/etc não fosse pra mim, afinal todos os outros entenderam a piada/gíria (ou ao menos parecem ter entendido).  
O que não me parece ser uma coisa boa, principalmente porque a cultura devops é extremamente inclusiva (ao menos do meu ponto de vista). 

## Verdade, já aconteceu comigo... Mas não tem jeito né? A pessoa tem de usar uma ferramenta de busca e tentar entender 
Não é bem assim, nem sempre fica na cara do que se trata o motivo de tanta gente estar rindo, já aconteceu de eu achar que tinha entendido a piada mas ela ia mais fundo, só fui entender mesmo ela quando descobri um "novo animal" e notei que a piada que eu vi meses atrás incluía isso... 

Além de que não se pode esperar que todo mundo fique o tempo todo pesquisando sobre toda e qualquer coisa, se vai facilitar a vida de alguém, vale a pena ser feito. :) 

Então eu resolvi listar alguns "animais" e o que eu entendi como sendo o seu significado. :p

## Mas será que realmente vai servir de algo isso aqui? 
Não que eu ache que isso vá salvar o mundo nem que alguém perdido entre as terminologias vá magicamente parar aqui, mas acho que se eu posso fazer algo sobre isso, porque não fazer? Vai que alguém acha interessante e começa a repassar quando nota que alguém está perdido? Ou resolva fazer a sua versão (afinal quanto mais pessoas escrevendo sobre algo, maior a chance disso se difundir)? Vai que alguém leia, note que falta algo ou que o meu entendimento está errado e deixa um comentário? Já me ajudou! :D

## Beleza, vamos a esses animais logo? Muito papo e pouca ação já
Ok, ok... Vamos lá :) apenas lembre que isso foi o que eu entendi dos termos (depois de pesquisar), sinta-se livre para discordar nos comentários e afins (é sério, ninguém sabe tudo 100% e da discussão amigável vem um grande crescimento de conhecimento). 

-    **Unicórnio (unicorn)**: Uma empresa que chegou em um certo ponto em que as coisas parecem mágicas, como se eles sempre tivessem feito as coisas desse jeito... Geralmente vejo isso sendo usado de forma negativa (ex.: ah, pra unicórnios feito a Amazon, Etsy e Google isso de DevOps é fácil... Aqui na minha empresa não funciona). 
-    **Cavalo (horse)**: Pode se dizer que é um unicórnio que ainda não criou chifre, brilho em volta dele e nem deixa uma trilha de arco-íris por onde passa... É uma empresa que está apenas começando a galgar os passos em DevOps (fato interessante: no [episódio 11 do Podcast Arrested DevOps](http://arresteddevops.com/11) foi mostrado que a Etsy não se considera um unicórnio, no máximo um "cavalo com uns brilhos"... E acredito que ao menos a maioria dos "unicórnios" se vê da mesma forma). 
-    **Bode (goat)**: Uma pessoa curiosa e que costuma passar dos seus limites organizacionais (que costuma querer participar dos processos de outras áreas) como bodes costumam fazer (bodes sempre estão tentando fugir da sua cerca e são grandes exploradores)... é como são chamadas as pessoas que costumam iniciar o processo de mudança de cultura na empresa. 

É, de animais que eu conheço é isso... 

## hahaha, sério? Todo esse papo antes e fala de 3 animais só? 
Calma, primeiro que eu nunca falei que conhecia muitos termos (depois posso colocar mais alguns que vi em um texto em inglês sobre isso mas nunca vi sendo usado na prática)... Segundo que eu disse "de animais que eu conheço é isso", não que era tudo :p

-    **Silo**: Representa um setor/equipe (ou mesmo uma filial) de uma empresa... Ou seja, uma pequena parte do todo que é o negócio. 
-    **DevOps hipster**: Aquele cara que conhece ferramentas mais novas que todo mundo, escala ambientes maiores que todo mundo, enfim... Aquele que quer apenas parecer mais legal do que realmente fazer mudanças no seu negócio.
-    **Pets**: No contexto de gerência de servidores, é o tratamento de hosts de forma individual, cuidadosa e inclusive carinhosa... Há todo um cuidado na escolha de nomes para os servidores, sua instalação e configuração é feita de forma manual e são difíceis de serem reproduzido em um novo servidor... Em um caso extremo, o pet pode virar uma "bola de neve" (snowflake). 
-    **Gado (cattle)**: É o oposto dos pets... São tratados "para o abate", ou seja, de forma automatizada e replicável... E não há muita preocupação sobre a possibilidade de um servidor ser removido. 
-    **Floco de neve (snowflake)**: Assim como um floco de neve, é um servidor completamente único e impossível de ser replicado. 

## Ei! Faltou o termo xyz! 
Ótimo! Coloca ele aí nos comentários que eu atualizo a lista (colocando os créditos pela contribuição :)). 
