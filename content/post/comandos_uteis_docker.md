+++
title = "Comandos úteis no docker"
date = "2016-10-19T11:06:28"
aliases = [ "/post/comandos_uteis_docker/" ]
categories = [ "Docker" ]
tags = [ "Docker", "one-liners", "comandos" ]
author = "somatorio"

+++
Seguem alguns comandos úteis pro [Docker](https://www.docker.com/), no momento são alguns [one-liners](https://en.wikipedia.org/wiki/One-liner_program), mas não serão limitados a isto (quando tiver novas idéias irei atualizar, ou caso alguém queira contribuir...)

- Remover todos os containers  
`docker rm -f $(docker ps -qa)`
- Remover os containers parados (status exited)  
`docker rm $(docker ps -qf status=exited)`
- Parar todos os containers  
`docker stop $(docker ps -q)`
- Remover todas as imagens locais  
`docker rmi $(docker images -qa)`
- Remove imagens sem tag (nome e tag = `<none>` no docker images)   
`docker rmi $(docker images -qf dangling=true)`
- Remove volumes "órfãos"  
`docker volume rm $(docker volume ls -qf dangling=true)`
- Mostra uso de recursos dos containers rodando (Contribuição: [Wellington Silva](http://dumpscerebrais.com))  
`docker stats $(docker ps --format {{.Names}})`
