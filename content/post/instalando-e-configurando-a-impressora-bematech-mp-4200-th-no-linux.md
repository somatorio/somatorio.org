+++
title = "Instalando e configurando a impressora Bematech MP-4200 TH no linux"
date = "2015-08-11T19:51:44"
aliases = [ "/2015/08/11/instalando-e-configurando-a-impressora-bematech-mp-4200-th-no-linux/", "/post/instalando-e-configurando-a-impressora-bematech-mp-4200-th-no-linux/" ]
categories = [ "cups" ]
tags = [ "ArchLinux", "Bematech", "cups", "Linux", "MP-4200 TH" ]
author = "somatorio"

+++

Vou explicar em duas versões, uma curta e prática e uma mais longa, mostrando todo o processo passado (assim caso não funcione a curta pode-se seguir os meus passos e descobrir onde está o problema)

Caso descubra algo novo, comente o que fez ou um link para o teu post com os passos e afins (assim como o farei no [blog do Rafael Sierra](http://blog.rafaelsdm.com/))

**Versão Texto muito longo, nemli nemlerei**:

  1. Instalar o driver da bematech (http://www.bematech.com.br/equipamentos/produto/mp-4200-th)
  2. Instalar libusb-0.1.so.4 32 bits, no caso do arch o pacote é libusb-compat, se o sistema for 64bits usar a versão do AUR (https://aur.archlinux.org/packages/lib32-libusb-compat)
  3. Alterar regra do udev de acordo com a dica do Rafael Sierra (http://blog.rafaelsdm.com/2013/08/configurando-impressora-bematech-mp.html)
  4. Caso o sistema seja 64bits: instalar libcups.so.2 e libcupsimage.so.2 32bits, no archlinux eles estão disponíveis no pacote lib32-libcups

**Versão completa (contando como cheguei à esse caminho, a ordem não está bem de acordo com o que eu segui no dia, e sim na ordem que fica menos caótica)**:

Instalar cups normalmente    
Instalar o driver da bematech (http://www.bematech.com.br/equipamentos/produto/mp-4200-th)    
Se deseperar porque o cups não encontra a impressora (tô brincando, mas cheguei perto com o tempo =p)  
Colocando o cups em modo debug e verificando o log (`grep -i 'bema' /var/log/cups/error_log`) após um `lpinfo -v` aparece:
```
  D [11/Aug/2015:10:23:40 -0300] [cups-deviced] Started backend /usr/lib/cups/backend/usbbema (PID 800)
  D [11/Aug/2015:10:23:40 -0300] [CGI] usbbema: error while loading shared libraries: libusb-0.1.so.4: cannot open shared object file: No such file or directory
  E [11/Aug/2015:10:23:40 -0300] [cups-deviced] PID 800 (usbbema) stopped with status 127!
```
Ou seja, o driver depende do libusb-0.1.so.4, instalar o libusb-compat, lembrando que como o driver é 32bits o libusb deve ser 32 bits também

No caso do archlinux 64bits usar a versão do AUR (https://aur.archlinux.org/packages/lib32-libusb-compat/)

Após instalar libusb-compat a mensagem do log muda
```
  D [11/Aug/2015:10:44:00 -0300] [cups-deviced] Started backend /usr/lib/cups/backend/usbbema (PID 3150)
  D [11/Aug/2015:10:44:00 -0300] [CGI] Usage: usbbemalci (1.3.0) job-id user title copies options [file]
  D [11/Aug/2015:10:44:00 -0300] [cups-deviced] PID 3150 (usbbema) exited with no errors.</pre>
```
Mas o `lpinfo -v` continua não encontrando a impressora, isso acontece porque a regra do udev para esta impressora está desatualizada.  
Seguindo a dica do Rafael Sierra (http://blog.rafaelsdm.com/2013/08/configurando-impressora-bematech-mp.html) deve-se alterar o arquivo **/etc/udev/rules.d/69-bema.rules** para o seguinte conteúdo (sim, apagar ou comentar tudo o que estiver lá e colocar isso)
```
  #MP4000TH
  SUBSYSTEM=="usb", ACTION=="add", ATTR{idVendor}=="0b1b", ATTR{idProduct}=="0001", MODE="0777"
  #MP4200TH
  SUBSYSTEM=="usb", ACTION=="add", ATTR{idVendor}=="0b1b", ATTR{idProduct}=="0003", MODE="0777"
```
Na dica do Rafael fala p/ colocar o `cdc_adm` na blacklist do modprobe, nos testes que eu fiz reconheceu a impressora mesmo sem fazer isso (caso continue sem reconhecer, teste colocar o `cdc_adm` na blacklist)

Agora o `lpinfo -v` reconhece a impressora

 `direct usbbema:/mp4200th/1`

Seguir a instalação normalmente no cups

Caso o sistema seja 64bits irá aparecer "stopped Filter failed" como status da impressão de teste (ou qualquer impressão que tentares), novamente o motivo é o driver ser 32bits...  
Seguindo a dica do William Merlotto (http://blog.rafaelsdm.com/2013/08/configurando-impressora-bematech-mp.html?showComment=1434564924587#c5009677282973103855) usei o ldd p/ ver as dependencias do rastertobema
```
  ldd /usr/lib/cups/filter/rastertobema
      linux-gate.so.1 (0xf76f5000)
      libcups.so.2 => not found
      libcupsimage.so.2 => not found
      libpthread.so.0 => /usr/lib32/libpthread.so.0 (0xf76be000)
      libc.so.6 => /usr/lib32/libc.so.6 (0xf7507000)
      libm.so.6 => /usr/lib32/libm.so.6 (0xf74b8000)
      /lib/ld-linux.so.2 (0xf76f6000)
```
Ambas as dependencias são sanadas com o lib32-libcups no caso do archlinux

Após a instalação do pacote testar a reimpressão, aqui funcionou normalmente desta forma
