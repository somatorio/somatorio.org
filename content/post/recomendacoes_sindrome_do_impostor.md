+++
title = "Recomendações de material sobre Síndrome do Impostor"
date = "2018-09-28T16:30:00"
categories = [ "DevOps" ]
tags = [ "DevOps", "empatia", "palestras" ]
author = "somatorio"
+++

Notei que um problema comum da [palestra]({{< relref "a-importancia-de-mais-palestras-nao-tecnicas.pt-br.md" >}}) sobre Síndrome do Impostor <sup>[Artigo pendente]</sup> é que são MUITOS links de referência e fica díficil passar pras pessoas de forma fácil em um slide, então resolvi colocar aqui tudo o que eu já li/vi sobre o assunto (assim só passo um link ;)), e a idéia é ir atualizando quando descobrir/"alguém me passar" outros :)

Obs: Algumas coisas podem não SER sobre Síndrome do Impostor mas são materiais que me ajudaram com isso, então eu recomendo igual ;P

### Artigos

*   [Artigo da pesquisa original - Pauline Rose Clance & Suzanne Imes - *The Imposter Phenomenon in High Achieving Women: Dynamics and Therapeutic Intervention*](http://www.paulineroseclance.com/pdf/ip_high_achieving_women.pdf)
*   [Kate Atkin - *What is impostor syndrome*](http://blog.oxforddictionaries.com/2017/04/what-is-impostor-syndrome)
*   [Lee Kolbert - *Imposter Syndrome: Equal Opportunity For All*](http://www.huffingtonpost.com/entry/imposter-syndrome-equal-opportunity-for-all_us_58078f79e4b08ddf9ece1328)
*   [Fernando Ike - *Blameless: a culpa não é sua*](http://medium.com/@fernandoike/blameless-a-culpa-n%C3%A3o-%C3%A9-sua-f2d8241595d0)
*   [Anna Borges - *17 dicas úteis para lidar com a Síndrome do Impostor*](http://www.buzzfeed.com/annaborges/17-dicas-geniais-para-lidar-com-a-sindrome-do-impo)
*   [Rosalind Adler and Lea Sellers - *How to banish 'imposter syndrome' once and for all*](https://www.telegraph.co.uk/women/womens-business/10517064/How-to-banish-imposter-syndrome-once-and-for-all.html)
*   [Melody Wilding - *The Five Types Of Impostor Syndrome And How To Beat Them*](http://www.fastcompany.com/40421352/the-five-types-of-impostor-syndrome-and-how-to-beat-them)
*   [Gina Rosenthal - *Can rock stars fuel impostor syndrome, and what you need to know to protect yourself*](https://24x7itconnection.com/2017/08/14/can-rock-stars-fuel-impostor-syndrome/)
*   [Melinda Gates - How to tackle impostor syndrome in the new year](https://www.linkedin.com/pulse/how-tackle-impostor-syndrome-new-year-melinda-gates/)
*   [Leah Donnella - *'Racial Impostor Syndrome': Here Are Your Stories*](https://www.npr.org/sections/codeswitch/2018/01/17/578386796/racial-impostor-syndrome-here-are-your-stories)
*   [Ulf Ehlert -  *The Dunning-Kruger effect in innovation*](https://understandinginnovation.blog/2015/07/03/the-dunning-kruger-effect-in-innovation/)
*   [*Let's Talk About 'Imposter Syndrome'*](https://www.thehumblelab.com/lets-talk-about-imposter-syndrome/#)
*   [Scott Hanselman - *I'm a phony. Are you?*](https://www.hanselman.com/blog/ImAPhonyAreYou.aspx)
*   [Eliza Inaê - *Síndrome do Impostor: O que é e como combatê-la?*](https://www.almanaquesos.com/sindrome-do-impostor-o-que-e-e-como-combate-la/)
*   [Taís Lenny - *Segundo a psicologia: quanto mais incompetente uma pessoa, mais confiante ela é*](https://www.almanaquesos.com/segundo-a-psicologia-quanto-mais-incompetente-uma-pessoa-mais-confiante-ela-e/)
*   [David Dunning - We are all confident idiots](https://psmag.com/social-justice/confident-idiots-92793)
*   [Eu (:p) - Síndrome do super-humano invertido]({{< relref "sindrome_do_super_humano_invertido.md" >}})
*   [Pablo Stanley](https://thedesignteam.io/the-imposter-ddbe96b14e14)

### Vídeos

*   [Natalie Portman Harvard Commencement Speech](http://www.youtube.com/watch?v=jDaZu_KEMCY)
*   [Jody Wolfborn - *You Don't Belong Here: Dealing with Impostor Syndrome*](http://www.youtube.com/watch?v=sAiPNASoMO4)
*   [Gitte Klitgaard - The Impostor Syndrome](http://www.infoq.com/presentations/impostor-syndrome-2)
*   [Gitte Klitgaard - The Impostor Syndrome](https://www.youtube.com/watch?v=vLpqq0ljawE)
*   [Nickolas Means - *You are Not an Impostor*](http://www.youtube.com/watch?v=l_Vqp1dPuPo)
*   [Joss Paling - *Feeling like a better developer: AKA overcoming impostor syndrome*](http://www.youtube.com/watch?v=cWZM_KgOFD4)
*   [Jacob Kaplan-Moss - *PyCon 2015 Keynote*](https://www.youtube.com/watch?v=hIJdFxYlEKE)
*   [Cynthia Zanoni - *Mulheres, precisamos falar de Síndrome do Impostor*](https://youtu.be/Lr_PlKjIZ3Y?t=26m8s)
*   [Angela Dugan - *Fear and (Self) Loathing in IT – A Healthy Discussion on Imposter Syndrome*](https://www.devopsdays.org/events/2016-chicago/program/angela-dugan/)
*   [Brené Brown - The Power of vulnerability](https://www.ted.com/talks/brene_brown_the_power_of_vulnerability)
*   [Brené Brown - Listening to shame](https://www.ted.com/talks/brene_brown_listening_to_shame#t-1206573)

### Podcasts

*   [10Deploys - *Episódio 12 - Síndrome do Impostor*](https://www.10deploys.com/episodios/012-sindrome-impostor)

### Testes

*   [Pauline Rose Clance - Clance Imposter Phenomenon scale test](http://www.paulineroseclance.com/pdf/IPTestandscoring.pdf)

### Livros

*   [Valerie Young - *Os Pensamentos Secretos das Mulheres de sucesso*](https://www.amazon.com/Pensamentos-Secretos-Mulheres-Portuguese-Brasil/dp/8502177591)
*   [Charles Duhigg - *O poder do hábito*](https://www.amazon.com.br/poder-h%C3%A1bito-fazemos-vida-neg%C3%B3cios-ebook/dp/B00A3D10JE)

Por enquanto é isso, mas estou sempre aceitando indicações de material. :P
Inclusive queria agradecer a todos que me ajudaram com indicações, tenho medo de esquecer de alguém então fica como um agradecimento geral, mas vocês sabem o quão grato eu sou por toda a ajuda.
