+++
title = "Síndrome do super humano invertido"
date = "2017-08-15T16:24:15"
categories = [ "Sem categoria" ]
tags = [ "Sindrome do impostor" ]
author = "somatorio"

+++
Já faz algum tempo que eu tenho uma sensação, e sinceramente tenho notado que não sou o único, então queria falar sobre isso e ao mesmo tempo que falo sobre as coisas que eu faço para melhorar essa sensação, espero receber feedbacks sobre como outros têm lidado com isso...

## E que sensação é essa?
Eu chamo ela de "síndrome do super humano invertido", mas não se apavore com o nome, é basicamente a sensação de que tu és "uma pessoa normal entre deuses"...

## Como assim? Explica melhor esse papo de "uma pessoa normal entre deuses".
É aquela sensação de que todos à tua volta (ou a grande maioria) estão num nível extremamente mais elevado que o teu, não importa o teu esforço... Como se algo te impedisse de subir, ou que essas pessoas simplesmente avançam a saltos enquanto tu avanças a passos.  
Eu vejo ela como uma extensão da [Síndrome do Impostor] (https://pt.wikipedia.org/wiki/S%C3%ADndrome_do_impostor) [(e se pesquisar sobre isso, vai ver que é extremamente comum, muitas pessoas famosas sofrem disso inclusive)] (http://www.huffpostbrasil.com/2014/04/07/7-sinais-de-que-voce-e-uma-das-vitimas-da-sindrome-do-impostor_a_21667908/).

## Ah, mas isso não é aquele esquema da [geração y] (http://qga.com.br/comportamento/jovem/2013/09/porque-os-jovens-profissionais-da-geracao-y-estao-infelizes)?
Quando penso sobre eles, acho que são parecidos, mas nesse caso tu te sente o inverso de ser especial, como se te faltasse algo, sei lá.  
Talvez seja algo que vem quando a pessoa percebe que ela não é tão especial quanto imagina.

## Saquei, acho que também tenho isso, o que tu acreditas que seja a causa?
Imagino que seja porque temos o costume de "olhar pra cima, e não pra baixo" quando nos auto-avaliamos, e só vemos as pessoas que já estão no ponto que tu queres chegar, e acabamos não notando as pessoas que querem chegar no "nosso" nível.

Há também o fato de que as pessoas tendem a não falar dos seus fracassos, só das vitórias, então acabamos vendo os outros exatamente como deuses perfeitos, que não cometem erros, não tem dúvidas e nunca falham.

## É, faz sentido... E como tu lidas com isso?
Basicamente conversando, tento ao máximo me lembrar de feedbacks positivos que recebi, algumas vezes exatamente dos tais "deuses" (que na verdade são amigos).  
Também ajuda lembrar que **todo mundo erra**: basta lembrar do que [aconteceu recentemente com a gitlab] (https://about.gitlab.com/2017/02/01/gitlab-dot-com-database-incident/) (inclusive o jeito que eles lidaram com o problema foi exemplar na minha opinião); do [episódio do WannaCry] (https://pt.wikipedia.org/wiki/WannaCry), que depois apareceram milhares de supostos especialistas dizendo que tudo podia ter sido evitado se os computadores estivessem atualizados (no início do ano a Microsoft tinha liberado um hotfix que resolvia essa vulnerabilidade), mas quase ninguém (inclusive empresas enormes) havia feito essa atualização; enfim, acredito que expliquei meu ponto, certo?

## E isso **resolveu** teu problema?
Não completamente, essa sensação acaba vindo de vez em quando, mas eu me sinto mais forte ao enfrentar ela. E quanto mais eu converso com as pessoas à minha volta (presencialmente ou pela internet), melhor me sinto em relação à minha situação.

Acredito que pouco a pouco isso vá diminuindo até passar de vez, perceber que outras pessoas passam por essa situação também ajuda, e espero que este texto ajude a ti também :)  
Caso tenha alguma outra sugestão fique livre para comentar, entrar em contato por email/telegram/etc, ou até para fazer um PR no [repositório do site](https://gitlab.com/somatorio/somatorio.org) :)

P.S.: Conversei com um amigo que estudou psicologia e ele disse que o que é descrito aqui é parte da Síndrome do Impostor mesmo.

P.S 2: Caso a síndrome passe a ser um grande empecilho na sua vida, indico a procura de um terapeuta para estudar e trabalhar seu caso.
