+++
title = "Hugo + GitLab + GitLab CI = win!"
date = "2016-10-12T10:34:44"
aliases = [ "/post/hugo_gitlab_gitlabci_win/" ]
categories = [ "blog" ]
tags = [ "Hugo", "GitLab", "CI", "Pipeline", "Automação" ]
author = "somatorio"

+++

O sistema do blog foi migrado do [WordPress](http://www.wordpress.org/) para [Hugo](http://www.gohugo.io) pois:  

1. Se o conteúdo do blog é completamente estático, porque ele precisa ser gerado dinamicamente?
2. Eu queria brincar com um sistema novo :p (já conhecia por cima o [jekyll](http://jekyllrb.com/), já que ele foi usado na lista do telegram)

## Como?

No início eu simplesmente converti o conteúdo do WordPress para *markdown*, gerei o conteúdo estático (html, estrutura dos diretórios, enfim...) usando uma [imagem](https://github.com/jojomi/docker-hugo) [Docker](http://www.docker.com) do Hugo e fiz upload do conteúdo da pasta `public/` para o meu host... Mas daí eu pensei "tem de ter um jeito automatizado de fazer isso..."

Como eu estava pensando em [versionar](http://www.scriptcase.com.br/blog/o-que-e-versionamento/) os arquivos do conteúdo (os *markdown* originais e tal, não o conteúdo html gerado) e queria brincar com os recursos do [gitlab](http://gitlab.com), aproveitei a chance e montei uma *pipeline* no [repositório](https://gitlab.com/somatorio/somatorio.org) usando o [CI/CD *built-in*](https://about.gitlab.com/gitlab-ci/)  do gitlab (não, não foi um *insight*  que virou algo pronto do nada, nunca tinha montado uma *pipeline*, e dei uma apanhada pra montar algo simples... Dá pra ver no histórico de commits :p)

A *pipeline* atual é bem simples, dividida em 2 estágios: *build* e *deploy*...

1. No *build* é gerado o conteúdo (usando a mesma imagem em Docker que eu usei no meu note a primeira vez)
2. No *deploy* é enviado o artefato gerado no *build* (o `public/`) via `rsync` para o host

Como é enviado por `rsync` eu precisei colocar o *host* no `known_hosts` (assim não rolava aquela questão sobre conectar em um host desconhecido) e a minha chave privada (assim não solicitaria senha) no  `~/.ssh/` do *runner*, mas como fazer isso se o *runner* é montado e destruído toda vez que a *pipeline* roda? E a questão de segurança, afinal não dá pra colocar os dados do meu host e a minha chave privada em um repositório público, né?
A resposta para o primeiro dilema foi o  `before_script`, assim antes de rodar qualquer script da pipeline ele define as configurações necessárias... E para o segundo, a resposta foram as *secret variables* do gitlab

Tem muita coisa que pode ser melhorada é claro (inclusive se alguém quiser enviar PR ;)),  como por exemplo um teste antes de fazer o deploy... Mas essa é a beleza do [CD](https://continuousdelivery.com) (porque CD? Uma hora falo sobre uma teoria minha :p), sempre tem algo para ser melhorado :)

## Tá, mas porque "= win"? Que vantagens tu tens com esse *setup*?

As que eu consigo pensar agora são:

- Qualquer pessoa pode contribuir (novos posts/páginas/correções/whatever) sem precisar de usuário no sistema, basta fazer PR
- Por serem apenas arquivos estáticos a carga fica mais rápida (afinal não precisa passar por App server, BD, etc...)
- Eu posso fazer todo o novo conteúdo offline (inclusive gerando previews) e "upar" quando estiver online... (de fato boa parte desse post foi feita offline)
- Como o conteúdo é versionado posso fazer rollback, comparar alterações, enfim... (sim, o WordPress tem isso, mas eu não perdi o recurso pois o git me proporciona ele)

Na verdade provavelmente o limite do que pode ser feito é a minha imaginação (e isso não é o caso ~~quase~~ sempre? Provavelmente, mas enfim :p)
