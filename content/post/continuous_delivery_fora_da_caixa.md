+++
title = "Continuous Delivery \"fora da caixa\""
date = "2016-10-14T19:34:23Z"
aliases = [ "/post/cd_fora_da_caixa/", "/post/continuous_delivery_fora_da_caixa/" ]
categories = [ "Continuous Delivery" ]
tags = [ "Tarefas", "Continuous Delivery", "CD" ]
author = "somatorio"

+++

Vou contar a história de 3 pessoas: Juca, Ana e Pedro.  
![Juca](/img/juca.png)
![Ana](/img/ana.jpeg)
![Pedro](/img/pedro.png)

> Eles estão fazendo um trabalho escolar e cada um pegou uma parte do trabalho pra fazer.  

> Assim que vão fazendo algo na sua parte do trabalho enviam aos seus colegas como está indo a sua parte, e vão corrigindo o que os colegas sugerem.  

> Como seu editor de texto possui verificação automática de ortografia, o texto vai sendo corrigido enquanto escrevem e eles se preocupam mais com o conteúdo do texto.

> Os três querem a melhor nota, então vão melhorando o trabalho até a data de entrega.

> Na hora da apresentação todos apresentam e fazem comentários em relação a algo que algum deles tenha esquecido de falar na sua parte, e fazem o mesmo na hora de resposta a perguntas (afinal a avaliação é do grupo inteiro =p)  

Tirando a parte do "mundo ideal onde tudo segue perfeitamente", já deves ter notado algo, certo? (claro que o título dá A dica)  
Na nossa história acima vemos os [princípios](https://continuousdelivery.com/principles/) de [CD](http://continuousdelivery.com) sendo aplicados, e (ao menos no meu caso) ela replica bem como é a preparação de um trabalho em grupo (ok, no mundo real é um pouco mais caótico - e em alguns casos vemos pessoas se escorando e tal, mas deu pra entender, né?)...  
Claro que vendo por essa perspectiva se notam várias melhorias que podiam ser feitas no processo, e é exatamente sobre isso que é esse texto!   

Muitas vezes não notamos, mas aplicamos (ou poderíamos aplicar) entrega contínua em outras tarefas que não sejam desenvolvimento de software, e exatamente por não percebermos isso acabamos deixando de incorporar práticas que trariam um melhor trabalho com maior eficiência. Pare pra pensar agora, em quantas tarefas (do trabalho ou não) podem ser aplicados os princípios de entrega contínua? Como podem ser aplicados esses princípios? Sou capaz de apostar que devem ter vindo várias idéias para melhorar o desenvolvimento dessas tarefas :)   
Caso este texto tenha deixado uma "semente" de uma melhor forma de se fazer as tarefas, eu considero que ter escrito ele já não foi perda de tempo :)

P.s.: - "Ah, mas esta historinha não se aplica no mundo real! Nos meus grupos (escolar/faculdade, trabalho, etc) todo mundo só se escora em um!"  
Bem, então em primeiro lugar isso não é um trabalho em grupo (afinal pra ser trabalho em grupo, ele meio que deve ser feito em grupo, certo?)... E em segundo lugar eu tenho um exemplo real :) o livro [Docker para desenvolvedores](https://leanpub.com/dockerparadesenvolvedores) aplica exatamente isso! De fato eu estava começando a ler sobre entrega contínua quando observei a forma que o [Rafael Gomes](http://techfree.com.br) publica o livro e notei que ele estava aplicando CD, o que gerou a idéia deste texto (o que já faz um tempo... Na época até comentei com ele sobre isso, só não sei se ele estava aplicando os princípios de maneira ciente ou não)
