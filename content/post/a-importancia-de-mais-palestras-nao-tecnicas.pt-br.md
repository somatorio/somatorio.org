+++
title = "Como foi palestrar na Campus Party Brasil 11 (ou: A importância de mais palestras não técnicas)"
date = "2018-02-02T19:14:00"
categories = [ "DevOps" ]
tags = [ "DevOps", "empatia", "palestras", "eventos", "cpbr" ]
author = "somatorio"
+++

Dia 31/01 eu tive a honra de palestrar na [Campus Party Brasil 11](http://brasil.campus-party.org) junto do [Fernando Ike](https://www.fernandoike.com/) falando sobre Síndrome do Impostor no palco Coders às 22h.

[Gravação](https://www.youtube.com/watch?v=hRhWmrweF0M)

[Slides](http://somatorio.org/talks/sindrome-impostor/)

## Sério? Como surgiu essa oportunidade?

Essa foi uma palestra com muitas pessoas envolvidas... Quando iniciaram as inscrições de palestra na Campus Party o [Gomex](https://gomex.me) já puxou papo comigo dizendo que eu tinha de aplicar, que se fosse o caso faria vaquinha com o pessoal pra que eu fosse, nisso ele conversou com o [Thalis Antunes](http://antunes.ninja) e ele sugeriu que eu fizesse a submissão pelo método ["Vire um curador"](http://brasil.campus-party.org/cpbr11/vire-um-curador/) pois caso passasse no processo teria as minhas despesas (ônibus de ida para SP e alimentação no evento) cobertas pela própria Campus, eu já conversava a tempos com o [Fernando Ike (fike)](https://www.fernandoike.com/) sobre Síndrome do Impostor e como isso nos afeta, então perguntei se ele topava palestrar comigo sobre o assunto e ele topou na hora, a [Lays](http://lays.space) junto com o pessoal do [Campusero's International Club](http://www.campuseros.club/) me deram o maior apoio na votação, isso sem falar nas pessoas que votaram nela me conhecendo ou não.

## Como foi a preparação da palestra?

Foram mais ou menos 2 meses trocando links e conversando com o fike sobre o assunto, comecei uma apresentação inicial (que estava beeeem fraquinha, hehe) e conversei com o fike sobre o que poderia ser melhorado, assim como comecei desde já a pedir feedback para algumas pessoas, como a minha noiva e o [Valdecir Carvalho](http://homelaber.com.br/) que contribuíram pra caramba na evolução da palestra, depois de VÁRIAS interações e alguns treinos de como seria a palestra, saiu a versão que foi apresentada na Campus (inclusive se pode ver o histórico da apresentação [aqui](https://gitlab.com/somatorio/slides-sindrome-impostor/commits/master))

## E a apresentação?

Algumas horas antes do horário da apresentação sentei com o fike e treinamos como seria, fui falando fingindo que estava de fato falando no palco (é, era só eu e ele e ainda assim eu falava "então pessoal", "entenderam?" e coisas do tipo), revimos algumas pequenas coisas para alterar nos slides e então tratamos de relaxar o resto do dia... Mesmo assim minutos antes da apresentação eu estava me sentindo extremamente ansioso, chegando a ter sinais físicos disso (meu estômago estava todo embrulhado, é sério, hahaha), depois de um tempo eu me acalmei um pouco e a coisa rolou.

O fike foi incrível participando junto e vendo as horas que eu me perdia ou deixava algo faltar, depois de um ponto eu não sentia mais como se estivesse apresentando uma palestra, e sim que estava conversando com amigos e explicando sobre algo (eu já sentia isso nas outras apresentações que fiz, mas nessa foi muito mais forte e "natural").

![Palco Coders CPBR11](/img/palcocoderscpbr11.jpg)
***A palestra foi no Palco Coders***

## E o público?

Achei incrível a quantidade de pessoas que se reuniram no palco para assistir, lotou todas as cadeiras e tinha bastante gente ficando lá de pé a palestra inteira pra assistir! Mesmo dizendo "129 participantes" na página do [campuse.ro](https://campuse.ro/events/campus-party-brasil-2018/talk/sindrome-do-impostor-voce-nao-esta-sozinho-devopsbr/) não imaginei que teria REALMENTE tanta gente p/ assistir lá na hora.

![Cadeiras Palco Coders](/img/cadeiraspalcocoderscpbr11.jpg)
***Acredite em mim, essas cadeiras estavam lotadas e tinha gente de pé atrás***

Adorei o quão participativo o pessoal foi, era visível a diferença no rosto das pessoas de acordo com o que iam se identificando com o que era apresentado, as perguntas feitas no final da palestra foram incríveis (e eu estava morrendo de medo do pessoal ficar em silêncio), e o pessoal ter vindo para junto do palco quando encerrou o tempo foi a realização de um sonho (verdade, tinha comentado disso com o fike umas horas antes, depois da apresentação que ele fez no espaço DevOps - DevOps Anti-Patterns, se ainda não viu, indico procurar)... Isso sem falar que todos os dias do evento ao menos uma pessoa vinha conversar comigo sobre a palestra, como se identificou, que achava que estava sozinho nessa, com perguntas sobre algumas coisas, enfim.

Aqui vão alguns testemunhos de pessoas que assistiram a palestra (vou atualizando de acordo com o que eu for recebendo):

* Francisco Cabral

> A palestra me fez pensar no meu dia-a-dia e me identifiquei com muitos dos pontos mostrados.
Estou trabalhando atualmente fora da cidade onde nasci, e sempre fico me perguntando se eu mereço estar aqui ou apenas consegui esse emprego por sorte. Sempre que faço algo errado fico muito mal, sem conseguir dormir e começo a trabalhar muito além da conta.
E a palestra me ajudou a ver que eu estou consumindo minha saúde mental e física diariamente com essas dúvidas, que na verdade são infundadas.
Ainda acho que estou com quem trabalho se encaixa na síndrome. Muitos colegas meus sofrem um pouco dos sintomas.

* [Carla Vieira](https://twitter.com/carlaprvieira)

> Tive o prazer de assistir essa palestra e muito obrigada novamente por terem trazido esse tema, espero que mais palestras não-técnicas apareçam na próxima campus. Esquecemos como nosso desenvolvimento pessoal é tão importante, como nossa saúde mental importa!

Pra mim isso só demonstrou o quanto o público de eventos de tecnologia ([mesmo com pessoas discordando sobre a CPBR ainda ser de tecnologia](https://www.linkedin.com/pulse/n%C3%B3s-perdemos-campus-party-s%C3%A3o-paulo-camila-fernandez-achutti/)) está carente de palestras "não técnicas", sobre tipos de problemas/situações/afins que passamos no nosso dia-a-dia e formas de lidar com isso. E os organizadores de eventos tem a oportunidade de alavancar mais palestras assim em seus eventos, como? Deixando claro que há oportunidades para palestras assim, chamando keynotes com palestras assim, enfim... Fazendo com que as pessoas se sintam mais à vontade para este tipo de palestra e que vejam que não é só de assuntos técnicos que eventos de tecnologia são feitos...

Quer um bom exemplo de prática? O pessoal que organiza o DevOpsDays Brasília divide a grade segundo o [CAMS](http://devopsdictionary.com/wiki/CAMS), basta ver na página da [programação do evento](http://devopsdays.bsb.br/about/programa/) para ver que há abertura (inclusive com slots reservados) para palestras sobre a parte cultural de DevOps e troca de experiências... Se adicionarem [ICE](http://radar.oreilly.com/2015/01/devops-keeps-it-cool-with-ice.html), ou melhor ainda, seguirem os [Three ways](https://itrevolution.com/the-three-ways-principles-underpinning-devops/) eu diria que fica ideal.

# Mais alguma coisa a dizer?

Bem, além das pessoas já citadas, queria deixar aqui meu agradecimento à organização da Campus Party pela oportunidade, ao pessoal de apoio no evento (staff, seguranças, enfim... vocês entenderam :P) e (por último mas não menos importante) às pessoas que pra mim são (possívelmente) os maiores heróis ocultos em eventos... o pessoal da limpeza do local, se o evento se manteve limpo e habitável foi pelo esforço incansável desses trabalhadores, sempre que eu conversei com eles me respondiam com um sorriso e pareciam sempre muito orgulhosos do que faziam (e pra minha não tão surpresa, pareciam surpreendidos por alguém conversar com eles, desejar um bom dia e agradecer pelo ótimo serviço).

Ahhhh, quase esqueci de falar... Fiz dois bons amigos no evento :)
![Camilla e Franscisco](/img/camillaefrancisco.jpg "Não precisa de muito tempo pra se ver que são amigos")